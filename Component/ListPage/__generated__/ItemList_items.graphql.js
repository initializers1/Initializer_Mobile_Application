/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
type ItemListItem_item$ref = any;
import type { FragmentReference } from "relay-runtime";
declare export opaque type ItemList_items$ref: FragmentReference;
declare export opaque type ItemList_items$fragmentType: ItemList_items$ref;
export type ItemList_items = {|
  +getItemDetails: ?{|
    +edges: ?$ReadOnlyArray<?{|
      +cursor: ?string,
      +node: ?{|
        +id: string,
        +categoryId: ?string,
        +subCategoryId: ?string,
        +$fragmentRefs: ItemListItem_item$ref,
      |},
    |}>,
    +pageInfo: ?{|
      +hasNextPage: boolean,
      +endCursor: ?string,
    |},
  |},
  +$refType: ItemList_items$ref,
|};
export type ItemList_items$data = ItemList_items;
export type ItemList_items$key = {
  +$data?: ItemList_items$data,
  +$fragmentRefs: ItemList_items$ref,
  ...
};
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "count"
    },
    {
      "kind": "RootArgument",
      "name": "itemType"
    },
    {
      "kind": "RootArgument",
      "name": "typeId"
    },
    {
      "kind": "RootArgument",
      "name": "userId"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "count",
        "cursor": "after",
        "direction": "forward",
        "path": [
          "getItemDetails"
        ]
      }
    ]
  },
  "name": "ItemList_items",
  "selections": [
    {
      "alias": "getItemDetails",
      "args": null,
      "concreteType": "ItemDetailsConnection",
      "kind": "LinkedField",
      "name": "__ItemList_getItemDetails_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ItemDetailsConnectionEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ItemDetails",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "id",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "categoryId",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "subCategoryId",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "ItemListItem_item"
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};
// prettier-ignore
(node/*: any*/).hash = '3661f6c65cf9953d5a949e41f84a8c64';

module.exports = node;
