import React, { useContext, useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  DeviceEventEmitter,
} from "react-native";
import { Button, ViewPager, Layout, Text } from "@ui-kitten/components";
import ImageOverlay from "../Extras/ImageOverlay";
import { KeyboardAvoidingView } from "../Extras/KeyboardAvoidingView";
import { MutationPageLoader } from "../Extras/Loaders";
import Constants from "expo-constants";
import Verification from "./Verification";
import GeneralInformation from "./GeneralInformation";

export default function Register({ route, navigation }) {
  const { AuthContext } = route.params;
  const { signUp, validateOtp, resendOtp } = useContext(AuthContext);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [emailState, setEmailState] = useState({ status: "control", msg: "" });
  const [passwordState, setPasswordState] = useState({
    status: "control",
    msg: "",
  });
  const [username, setUsername] = useState("");
  const [otp, setOtp] = useState("");
  const [pageLoadingState, setPageLoadingState] = useState({
    loading: false,
    display: false,
    msg: "",
    borderColor: "",
    backgroundColor: "",
  });
  const [pageState, setPageState] = useState("");
  const otpMaxExpiryTime = useRef(10);
  useEffect(() => {
    var registerListener = DeviceEventEmitter.addListener(
      "registerMsg",
      (e) => {
        setPageLoadingState(e);
      }
    );
    var validateOTPListener = DeviceEventEmitter.addListener(
      "validateOtp",
      (event) => {
        otpMaxExpiryTime.current = event.data;
        setPageState("OTP");
        setPageLoadingState({
          loading: false,
          display: true,
          msg: "OTP has sent to your mail, Enter OTP to continue",
          borderColor: "#414ee8",
          backgroundColor: "#636ff4",
        });
      }
    );
    return function cleanup() {
      registerListener.remove();
      validateOTPListener.remove();
    };
  }, []);
  const resetPageMessage = () => {
    setPageLoadingState({
      loading: false,
      display: false,
      msg: "",
      borderColor: "",
      backgroundColor: "",
    });
  };

  const onSignUpConfirm = () => {
    if (pageState === "") {
      if (!email || !validateEmail()) {
        setEmailState({ status: "danger", msg: "enter a valid email id" });
        return;
      } else {
        setEmailState({ status: "control", msg: "" });
      }
      if (!password) {
        setPasswordState({ status: "danger", msg: "enter your password" });
        return;
      } else {
        setPasswordState({ status: "control", msg: "" });
      }
      signUp({ username, email, password });
    } else {
      validateOtp({ otp: otp });
    }
  };
  //   const onResendOtp = () => {
  //     setPageLoadingState({
  //       loading: false,
  //       display: true,
  //       msg: "OTP has resent to your mail, Enter OTP to continue",
  //       borderColor: "#414ee8",
  //       backgroundColor: "#636ff4",
  //     });
  //   };
  const validateEmail = () => {
    var emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(email);
  };

  const onGoBack = () => {
    setPageState("");
    resetPageMessage();
  };

  const onSignInButtonPress = () => {
    navigation && navigation.navigate("Login");
  };
  if (pageLoadingState?.loading) {
    return <MutationPageLoader />;
  } else {
    return (
      <KeyboardAvoidingView>
        <ImageOverlay
          style={styles.container}
          source={require("../../assets/authpage.png")}
        >
          <View style={styles.headerContainer}>
            <Text category="h1" status="control">
              {Constants.manifest.name}
            </Text>
            <Text style={styles.signInLabel} category="s1" status="control">
              Create a new account
            </Text>
          </View>
          {pageLoadingState.display ? (
            <View
              style={{
                borderRadius: 4,
                margin: 4,
                marginHorizontal: 10,
                paddingHorizontal: 10,
                padding: 4,
                borderColor: pageLoadingState.borderColor
                  ? pageLoadingState.borderColor
                  : "red",
                backgroundColor: pageLoadingState.backgroundColor
                  ? pageLoadingState.backgroundColor
                  : "#a8605e",
                borderWidth: 1,
              }}
            >
              <Text style={{ color: "white" }} category="s1">
                {pageLoadingState?.msg}
              </Text>
            </View>
          ) : (
            <></>
          )}
          {pageState === "" ? (
            <GeneralInformation
              email={email}
              setEmail={setEmail}
              emailState={emailState}
              password={password}
              setPassword={setPassword}
              passwordState={passwordState}
              username={username}
              setUsername={setUsername}
            />
          ) : (
            <Verification
              email={email}
              otpMaxExpiryTime={otpMaxExpiryTime.current}
              resendOtp={resendOtp}
              onGoBack={onGoBack}
              otp={otp}
              setOtp={setOtp}
            />
          )}
          <Button
            style={styles.signInButton}
            status="control"
            size="giant"
            onPress={onSignUpConfirm}
          >
            {pageState === "OTP" ? "Confirm" : "Continue"}
          </Button>
          <Button
            style={styles.signUpButton}
            appearance="ghost"
            status="control"
            onPress={onSignInButtonPress}
          >
            Already have an account? Sign In
          </Button>
        </ImageOverlay>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    minHeight: 216,
  },
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
  signInLabel: {
    marginTop: 16,
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  forgotPasswordContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  emailInput: {
    marginTop: 16,
  },
  passwordInput: {
    marginTop: 16,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
});
