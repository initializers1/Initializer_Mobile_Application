import React, { useState } from "react";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";
import { Input, Icon, Text } from "@ui-kitten/components";

export default function GeneralInformation(props) {
  const {
    email,
    setEmail,
    emailState,
    password,
    setPassword,
    passwordState,
    username,
    setUsername,
  } = props;
  const [passwordVisible, setPasswordVisible] = useState(false);
  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={onPasswordIconPress}>
      <Icon {...props} name={passwordVisible ? "eye" : "eye-off"} />
    </TouchableWithoutFeedback>
  );
  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };
  const renderCaption = (message) => {
    if (!message) {
      return <></>;
    } else {
      return (
        <Text style={{ color: "#973a38" }} category="label">
          {message}
        </Text>
      );
    }
  };
  return (
    <View style={styles.formContainer}>
      <Input
        status="control"
        placeholder="UserName"
        autoCompleteType="name"
        value={username}
        onChangeText={(nextText) => setUsername(nextText)}
      />
      <Input
        style={styles.emailInput}
        placeholder="Email"
        autoCompleteType="email"
        keyboardType="email-address"
        value={email}
        onChangeText={(nextText) => setEmail(nextText)}
        status={emailState.status}
        caption={() => {
          return renderCaption(emailState.msg);
        }}
      />
      <Input
        style={styles.passwordInput}
        placeholder="Password"
        autoCompleteType="password"
        accessoryRight={renderIcon}
        value={password}
        secureTextEntry={!passwordVisible}
        onChangeText={setPassword}
        status={passwordState.status}
        caption={() => {
          return renderCaption(passwordState.msg);
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
  emailInput: {
    marginTop: 16,
  },
  passwordInput: {
    marginTop: 16,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
});
