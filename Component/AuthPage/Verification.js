import React, { useEffect, useState, useRef } from "react";
import { View, StyleSheet } from "react-native";
import {
  Input,
  TopNavigation,
  TopNavigationAction,
  Icon,
  Text,
  Button,
} from "@ui-kitten/components";

export default function Verification(props) {
  const { email, otpMaxExpiryTime, resendOtp, onGoBack, otp, setOtp } = props;
  const [enableResend, setEnableResend] = useState(false);
  const [counter, setCounter] = useState({ min: 0, sec: 0 });

  var otpExpiryTime = new Date();
  if (otpMaxExpiryTime) {
    otpExpiryTime.setSeconds(otpExpiryTime.getSeconds() + otpMaxExpiryTime);
  }
  const totalTime = useRef(otpExpiryTime);

  const changeRemainingTime = () => {
    var remainingTime = totalTime.current - new Date();
    if (remainingTime > 0) {
      var sec = Math.floor((remainingTime / 1000) % 60);
      var min = Math.floor((remainingTime / 1000 / 60) % 60);
      var cont = {
        min: String(min).padStart(2, "0"),
        sec: String(sec).padStart(2, "0"),
      };
      setCounter({
        min: String(min).padStart(2, "0"),
        sec: String(sec).padStart(2, "0"),
      });
    } else {
      setEnableResend(true);
    }
  };
  useEffect(() => {
    setTimeout(() => {
      changeRemainingTime();
    }, 1000);
  }, [counter]);

  const onResendOTP = () => {
    setEnableResend(false);
    otpExpiryTime = new Date();
    otpExpiryTime.setSeconds(otpExpiryTime.getSeconds() + otpMaxExpiryTime);
    totalTime.current = otpExpiryTime;
    changeRemainingTime();
    resendOtp();
  };
  const BackIcon = (props) => (
    <Icon fill="white" {...props} name="arrow-back" />
  );
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={onGoBack} />
  );
  const BackTitle = () => (
    <Text category="s1" style={{ color: "white" }} onPress={onGoBack}>
      {email}
    </Text>
  );
  return (
    <View style={styles.formContainer}>
      <TopNavigation
        accessoryLeft={BackAction}
        style={{ backgroundColor: "transparent" }}
        title={BackTitle}
      />
      <Input
        placeholder="Enter OTP"
        autoCompleteType="cc-number"
        keyboardType="number-pad"
        value={otp}
        onChangeText={setOtp}
        status="control"
      />
      {enableResend ? (
        <View style={{ alignSelf: "flex-start" }}>
          <Button appearance="ghost" status="control" onPress={onResendOTP}>
            Resend OTP
          </Button>
        </View>
      ) : counter?.min ? (
        <View
          style={{
            flexDirection: "row",
            marginVertical: 12,
            marginHorizontal: 10,
          }}
        >
          <Text category="label" style={{ color: "white" }}>
            {"Resend otp in "}
          </Text>
          <Text category="label" style={{ color: "white" }}>
            {`${counter.min}: ${counter.sec}`}
          </Text>
        </View>
      ) : (
        <></>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
});
