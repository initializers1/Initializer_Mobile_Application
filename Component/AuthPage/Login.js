import React, { useContext, useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  DeviceEventEmitter,
} from "react-native";
import { Button, Input, Text, Icon } from "@ui-kitten/components";
import ImageOverlay from "../Extras/ImageOverlay";
import { KeyboardAvoidingView } from "../Extras/KeyboardAvoidingView";
import { MutationPageLoader } from "../Extras/Loaders";
import Constants from "expo-constants";

export default function Login({ route, navigation }) {
  const { AuthContext } = route.params;
  const { signIn } = useContext(AuthContext);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [emailState, setEmailState] = useState({ status: "control", msg: "" });
  const [passwordState, setPasswordState] = useState({
    status: "control",
    msg: "",
  });
  const [pageState, setPageState] = useState({
    loading: false,
    display: false,
    msg: "",
  });

  useEffect(() => {
    var eventListener = DeviceEventEmitter.addListener("loginError", (e) => {
      setPageState(e);
    });
    return function cleanup() {
      eventListener.remove();
    };
  }, []);
  const onSignInButtonPress = () => {
    if (!email || !validateEmail()) {
      setEmailState({ status: "danger", msg: "enter a valid email id" });
      return;
    } else {
      setEmailState({ status: "control", msg: "" });
    }
    if (!password) {
      setPasswordState({ status: "danger", msg: "enter your password" });
      return;
    } else {
      setPasswordState({ status: "control", msg: "" });
    }
    signIn({ email, password });
  };

  const validateEmail = () => {
    var emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(email);
  };

  const onSignUpButtonPress = () => {
    navigation && navigation.navigate('Register');
  };

  const onForgotPasswordButtonPress = () => {
    // navigation && navigation.navigate('ForgotPassword');
  };

  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };
  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={onPasswordIconPress}>
      <Icon {...props} name={passwordVisible ? "eye" : "eye-off"} />
    </TouchableWithoutFeedback>
  );
  const renderCaption = (message) => {
    return (
      <Text style={{ color: "#973a38" }} category="label">
        {message}
      </Text>
    );
  };
  if (pageState?.loading) {
    return <MutationPageLoader />;
  } else {
    return (
      <KeyboardAvoidingView>
        <ImageOverlay
          style={styles.container}
          source={require("../../assets/authpage.png")}
        >
          <View style={styles.headerContainer}>
            <Text category="h1" status="control">
              {Constants.manifest.name}
            </Text>
            <Text style={styles.signInLabel} category="s1" status="control">
              Sign in to your account
            </Text>
          </View>
          {pageState.display ? (
            <View
              style={{
                borderRadius: 4,
                margin: 4,
                marginHorizontal: 10,
                paddingHorizontal: 10,
                padding: 4,
                borderColor: "red",
                backgroundColor: "#a8605e",
                borderWidth: 1,
              }}
            >
              <Text style={{ color: "white" }} category="s1">
                {pageState?.msg}
              </Text>
            </View>
          ) : (
            <></>
          )}
          <View style={styles.formContainer}>
            <Input
              status="control"
              placeholder="Email"
              autoCompleteType="email"
              keyboardType="email-address"
              value={email}
              onChangeText={(nextText) => setEmail(nextText)}
              status={emailState.status}
              caption={() => {
                return renderCaption(emailState.msg);
              }}
            />
            <Input
              style={styles.passwordInput}
              placeholder="Password"
              autoCompleteType="password"
              accessoryRight={renderIcon}
              value={password}
              secureTextEntry={!passwordVisible}
              onChangeText={setPassword}
              status={passwordState.status}
              caption={() => {
                return renderCaption(passwordState.msg);
              }}
            />
            <View style={styles.forgotPasswordContainer}>
              <Button
                style={styles.forgotPasswordButton}
                appearance="ghost"
                status="control"
                onPress={onForgotPasswordButtonPress}
              >
                Forgot your password?
              </Button>
            </View>
          </View>
          <Button
            style={styles.signInButton}
            status="control"
            size="giant"
            onPress={onSignInButtonPress}
          >
            SIGN IN
          </Button>
          <Button
            style={styles.signUpButton}
            appearance="ghost"
            status="control"
            onPress={onSignUpButtonPress}
          >
            Don't have an account? Sign Up
          </Button>
        </ImageOverlay>
      </KeyboardAvoidingView>
    );
  }
}
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    minHeight: 216,
  },
  formContainer: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 16,
  },
  signInLabel: {
    marginTop: 16,
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  forgotPasswordContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  passwordInput: {
    marginTop: 16,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
});
