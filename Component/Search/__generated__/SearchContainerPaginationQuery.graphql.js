/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type SearchContainer_search$ref = any;
export type SearchContainerPaginationQueryVariables = {|
  loadQuery?: ?boolean,
  searchTerm: string,
|};
export type SearchContainerPaginationQueryResponse = {|
  +$fragmentRefs: SearchContainer_search$ref
|};
export type SearchContainerPaginationQuery = {|
  variables: SearchContainerPaginationQueryVariables,
  response: SearchContainerPaginationQueryResponse,
|};
*/


/*
query SearchContainerPaginationQuery(
  $loadQuery: Boolean
  $searchTerm: String!
) {
  ...SearchContainer_search
}

fragment SearchContainer_search on Query {
  searchAsList(searchTerm: $searchTerm) @include(if: $loadQuery) {
    id
    typeId
    name
    description
    type
    image
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "loadQuery"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "searchTerm"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "SearchContainerPaginationQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "SearchContainer_search"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "SearchContainerPaginationQuery",
    "selections": [
      {
        "condition": "loadQuery",
        "kind": "Condition",
        "passingValue": true,
        "selections": [
          {
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "searchTerm",
                "variableName": "searchTerm"
              }
            ],
            "concreteType": "ClientSearchResult",
            "kind": "LinkedField",
            "name": "searchAsList",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "typeId",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "image",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ]
      }
    ]
  },
  "params": {
    "cacheID": "6317c0649334940f3b003951a1deb5ee",
    "id": null,
    "metadata": {},
    "name": "SearchContainerPaginationQuery",
    "operationKind": "query",
    "text": "query SearchContainerPaginationQuery(\n  $loadQuery: Boolean\n  $searchTerm: String!\n) {\n  ...SearchContainer_search\n}\n\nfragment SearchContainer_search on Query {\n  searchAsList(searchTerm: $searchTerm) @include(if: $loadQuery) {\n    id\n    typeId\n    name\n    description\n    type\n    image\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '50567b12bd18f63ee63a7bde039a393a';

module.exports = node;
