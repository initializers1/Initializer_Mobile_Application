/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type SearchContainer_search$ref = any;
export type SearchQueryAppQueryVariables = {|
  searchTerm: string,
  loadQuery: boolean,
|};
export type SearchQueryAppQueryResponse = {|
  +$fragmentRefs: SearchContainer_search$ref
|};
export type SearchQueryAppQuery = {|
  variables: SearchQueryAppQueryVariables,
  response: SearchQueryAppQueryResponse,
|};
*/


/*
query SearchQueryAppQuery(
  $searchTerm: String!
  $loadQuery: Boolean!
) {
  ...SearchContainer_search
}

fragment SearchContainer_search on Query {
  searchAsList(searchTerm: $searchTerm) @include(if: $loadQuery) {
    id
    typeId
    name
    description
    type
    image
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "loadQuery"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "searchTerm"
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "SearchQueryAppQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "SearchContainer_search"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "SearchQueryAppQuery",
    "selections": [
      {
        "condition": "loadQuery",
        "kind": "Condition",
        "passingValue": true,
        "selections": [
          {
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "searchTerm",
                "variableName": "searchTerm"
              }
            ],
            "concreteType": "ClientSearchResult",
            "kind": "LinkedField",
            "name": "searchAsList",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "typeId",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "image",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ]
      }
    ]
  },
  "params": {
    "cacheID": "a39bf28dd90c0d23c15fed5a24a3a1bf",
    "id": null,
    "metadata": {},
    "name": "SearchQueryAppQuery",
    "operationKind": "query",
    "text": "query SearchQueryAppQuery(\n  $searchTerm: String!\n  $loadQuery: Boolean!\n) {\n  ...SearchContainer_search\n}\n\nfragment SearchContainer_search on Query {\n  searchAsList(searchTerm: $searchTerm) @include(if: $loadQuery) {\n    id\n    typeId\n    name\n    description\n    type\n    image\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '606dd6a70c6451c54664de83b7f9812c';

module.exports = node;
