/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type SearchContainer_search$ref: FragmentReference;
declare export opaque type SearchContainer_search$fragmentType: SearchContainer_search$ref;
export type SearchContainer_search = {|
  +searchAsList?: ?$ReadOnlyArray<?{|
    +id: string,
    +typeId: string,
    +name: ?string,
    +description: ?string,
    +type: ?string,
    +image: ?string,
  |}>,
  +$refType: SearchContainer_search$ref,
|};
export type SearchContainer_search$data = SearchContainer_search;
export type SearchContainer_search$key = {
  +$data?: SearchContainer_search$data,
  +$fragmentRefs: SearchContainer_search$ref,
  ...
};
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "loadQuery"
    },
    {
      "kind": "RootArgument",
      "name": "searchTerm"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "refetch": {
      "connection": null,
      "fragmentPathInResult": [],
      "operation": require('./SearchContainerPaginationQuery.graphql.js')
    }
  },
  "name": "SearchContainer_search",
  "selections": [
    {
      "condition": "loadQuery",
      "kind": "Condition",
      "passingValue": true,
      "selections": [
        {
          "alias": null,
          "args": [
            {
              "kind": "Variable",
              "name": "searchTerm",
              "variableName": "searchTerm"
            }
          ],
          "concreteType": "ClientSearchResult",
          "kind": "LinkedField",
          "name": "searchAsList",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "typeId",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "description",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "type",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "image",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ]
    }
  ],
  "type": "Query",
  "abstractKey": null
};
// prettier-ignore
(node/*: any*/).hash = '50567b12bd18f63ee63a7bde039a393a';

module.exports = node;
