import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Input, List, ListItem } from "@ui-kitten/components";
import { useNavigation } from "@react-navigation/native";
import { Search } from "../Extras/Icons";
import { AppColor } from "../Extras/Colors";
import { useRefetchableFragment, fetchQuery, useQueryLoader } from "react-relay/hooks";
import { } from 'react-relay';
import SearchQuery from './__generated__/SearchQueryAppQuery.graphql';
import RelayEnvironment from '../../GraphQLUtils/RelayEnvironment';
import {ITEMS_PER_PAGE} from '../Extras/Constants';


export default function SearchContainer(props) {
  const navigation = useNavigation();
  const renderItem = ({ item, index }) => {
    return (
      <ListItem key={index} title={item.name} description={item.description} accessoryRight={Search} onPress={() => onItemSelect({"typeId": item.typeId, "type": item.type})} />
    )
  };
  const onItemSelect = (event) => {
    console.log(event.typeId);
    if(event && event.type === "ItemDetails") {
      navigation.navigate('SearchNavigator', {
        screen: 'Details',
        params: { typeId: event.typeId },
      });
      // navigation.navigate('SearchNavigator', { typeId: event.typeId });
    }
  }
  const searchItem = (searchTerm) => {
    if(searchTerm.length > 2) {      
      fetchQuery(RelayEnvironment, SearchQuery, {
        searchTerm: searchTerm,
        count: ITEMS_PER_PAGE,
        after: 0,
        loadQuery: true
      }).subscribe({
        complete: () => {
          refetch({
            searchTerm: searchTerm,
            loadQuery: true
          }, 
          { fetchPolicy: "store-only", force: true });
        },
        error: (error) => {
          console.log("err",error);
        },
      });
    }
  }
  let [ data, refetch ] =
  useRefetchableFragment(
      graphql`
        fragment SearchContainer_search on Query
        @refetchable(queryName: "SearchContainerPaginationQuery") {
          searchAsList(searchTerm: $searchTerm)
          @include(if: $loadQuery) {
            id
            typeId
            name
            description
            type
            image
      }
        }
      `,
      props.search
    );
  console.log(data);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "transparent",
        justifyContent: "flex-end",
        flexDirection: "column",
      }}
    >
      <TouchableOpacity
        style={{
          flex: 0.2,
          alignItems: "center",
          justifyContent: "center",
          width: "100%",
          backgroundColor: "transparent",
        }}
        onPress={() => navigation.goBack()}
      ></TouchableOpacity>
      <View
        style={{
          flex: 0.8,
          alignItems: "center",
          width: "100%",
          backgroundColor: AppColor.Dull,
        }}
      >
        <Input
          style={{ alignSelf: "flex-start", margin: 10 }}
          size="large"
          placeholder="Search for items..."
          placeholderTextColor={AppColor.Dull}
          accessoryRight={() => Search(AppColor.Vibrant)}
          onChangeText={(searchTerm) => searchItem(searchTerm)}
        />
        <List
          style={{ width: "100%" }}
          data={data.searchAsList? data.searchAsList : []}
          renderItem={renderItem}
          onScrollBeginDrag={false}
        />
      </View>
    </View>
  );
}
