import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HeaderRight from '../Header/HeaderRight';
import Header from '../Header/Header';
import {AppColor} from '../Extras/Colors';

const SearchStack = createStackNavigator();

import SearchQuery from './SearchQuery';
import ItemDetailQuery from '../Items/ItemDetail/ItemDetailsQuery';

export default function SearchNavigator() {
  return (
    <SearchStack.Navigator
      screenOptions={{
        headerTitle: () => <Header />,
        headerRight: () => <HeaderRight/>,
        headerTitleAlign : 'left',
        headerBackTitle: '',
        headerStyle: {
          backgroundColor: AppColor.Vibrant,
        },
      }}
    >
      <SearchStack.Screen name="Details" component={ItemDetailQuery}/>
    </SearchStack.Navigator>
  );
}