import React from "react";
import { QueryRenderer, graphql } from "react-relay";
import { Text } from "@ui-kitten/components";
import SearchContainer from "./SearchContainer";
import RelayEnvironment from "../../GraphQLUtils/RelayEnvironment";
import { HomePageLoader } from "../Extras/Loaders";

export default function SearchQuery({ route }) {
  return (
    <>
      <QueryRenderer
        environment={RelayEnvironment}
        query={graphql`
          query SearchQueryAppQuery(
            $searchTerm: String!
            $loadQuery: Boolean!
          ) {
            ...SearchContainer_search
          }
        `}
        variables={{
          searchTerm: "",
          loadQuery: false,
        }}
        render={({ error, props }) => {
          console.log(error);
          if (error) {
            return <Text>Error!</Text>;
          }
          if (!props) {
            return <HomePageLoader />;
          }
          return <SearchContainer search={props} />;
        }}
      />
    </>
  );
}
