import React from "react";
import { Asset } from "expo-asset";
import AnimatedSplashScreen from "./AnimatedSplashScreen";
import AppLoading from "expo-app-loading";

export default function AnimatedAppLoader({ children, image }) {
  const [isSplashReady, setSplashReady] = React.useState(false);

  const startAsync = async () => {
    Asset.fromURI(image).downloadAsync();
    await Asset.fromModule(
      require("../../assets/authpage.png")
    ).downloadAsync();
  };

  const onFinish = React.useMemo(() => setSplashReady(true), []);

  if (!isSplashReady) {
    return (
      <AppLoading
        // Instruct SplashScreen not to hide yet, we want to do this manually
        autoHideSplash={false}
        startAsync={startAsync}
        onError={console.error}
        onFinish={onFinish}
      />
    );
  }

  return <AnimatedSplashScreen image={image}>{children}</AnimatedSplashScreen>;
}
