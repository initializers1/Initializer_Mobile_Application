# Initializers Mobile Application

# Overview
Created as part of Initializers Mobile commerse solution, application helps end users to browse through item catalog, add items to their wishlist & cart and complete their order & pay. Application supports dynamic home page layout, over the air update which can be configured through the admin app. More details on admin app can be found [here](https://gitlab.com/initializers1/Initializer_AdminUI).
# Built On
   1. [Expo](https://expo.dev/)
   2. [React Native](https://reactnative.dev/)
   3. [GraphQL](https://graphql.org/)
   4. [React Relay](https://relay.dev/)
   5. [UI Kitten](https://akveo.github.io/react-native-ui-kitten/)
# User Flow 
 * Authentication <br />
      Provides option to create new user and login with existing credentials. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610543/Mobile/Login_wrx9wo.png" width=20% height=20%>
 * HomePage <br />
      Pre configured home page supported with different widgets to display data at catagory, sub-catagory and item level. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610418/Mobile/Homepage_zmidrq.png" width=20% height=20%>
 * Details Page <br />
      Provides detailed information on the items which includes price, available quantities, item info, and media. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610545/Mobile/ItemDetails_qk1t69.png" width=20% height=20%>
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610547/Mobile/ItemDetails1_bltula.png" width=20% height=20%>
 * Wishlist <br />
      Personalized wishlist for the users to keep track of items. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610550/Mobile/Wishlist_hgag9w.png" width=20% height=20%>
 * Cart <br />
      Cart to add all the items before procceding to order. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610541/Mobile/Cart_fbaxqs.png" width=20% height=20%>
 * Processing Order <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610546/Mobile/ProcessOrder_mumxca.png" width=20% height=20%>
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610545/Mobile/ProcessOrder1_mby4oe.png" width=20% height=20%>
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610550/Mobile/OrderCompletion_dthq3i.png" width=20% height=20%>
 * Order Details <br />
      Provides users a detailed information of the orders, along with capabilities to track the order. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610543/Mobile/OrderList_rlc3jm.png" width=20% height=20%>
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610550/Mobile/OrderDetails_vk9n4b.png" width=20% height=20%>
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610542/Mobile/OrderDetails1_a3vdrc.png" width=20% height=20%>
 * User Info <br />
      Helps users to maintain personal information and address details. <br />
   <img src="https://res.cloudinary.com/dsywyhhdl/image/upload/v1670610549/Mobile/UserInfo_y0418y.png" width=20% height=20%>
    
# Links
1. [Initializers API](https://gitlab.com/initializers1/initializers_api)
2. [Initializers Admin UI](https://gitlab.com/initializers1/Initializer_AdminUI)
