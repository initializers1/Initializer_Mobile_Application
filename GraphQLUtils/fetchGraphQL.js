import Constants from "expo-constants";
import * as SecureStore from "expo-secure-store";

async function fetchGraphQL(text, variables) {
  const accessToken = await SecureStore.getItemAsync(
    "initializers.accessToken"
  );
  const response = await fetch(
    `${Constants.manifest.extra.accessURL}/graphql`,
    {
      method: "POST",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query: text,
        variables,
      }),
    }
  );

  // Get the response as JSON
  return await response.json();
}

export default fetchGraphQL;
