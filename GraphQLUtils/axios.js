import Axios from "axios";
import Constants from "expo-constants";

const instance = Axios.create({ baseURL: Constants.manifest.extra.accessURL });
instance.interceptors.request.use(
  (req) => {
    // req.headers.common["Authorization"] = "Bearer " + accessToken;
    req.headers.common["X-Tenant"] = Constants.manifest.android.package;
    return req;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default instance;
