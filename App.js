import React, { useState, useReducer, useEffect, useMemo, useRef } from "react";
import { StyleSheet, DeviceEventEmitter } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { RelayEnvironmentProvider, useQueryLoader } from "react-relay/hooks";

import Constants from "expo-constants";
import * as SecureStore from "expo-secure-store";

import * as eva from "@eva-design/eva";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import { ApplicationProvider, IconRegistry } from "@ui-kitten/components";

import RelayEnvironment from "./GraphQLUtils/RelayEnvironment";
import FeedQuery from "./Component/Feeds/__generated__/FeedsContainerAppQuery.graphql";
import SearchQuery from "./Component/Search/SearchQuery";

import Home from "./Component/Home/Home";
import Login from "./Component/AuthPage/Login";
import Register from "./Component/AuthPage/Register";
import PaymentPage from "./Component/Payment/PaymentPage";
import SearchNavigator from "./Component/Search/SearchNavigator";

import AnimatedAppLoader from "./Component/StartPage/AnimatedAppLoader";

import { AppColor } from "./Component/Extras/Colors";
import { HomePageLoader } from "./Component/Extras/Loaders";
import { INITAIL_PAGINATION_COUNT } from "./Component/Extras/Constants";
import { keyUserId, keyAccessToken } from "./Component/Extras/Keys";
import { MutationPageLoader } from "./Component/Extras/Loaders";

import axios from "./GraphQLUtils/axios";

const Stack = createStackNavigator();
const AuthContext = React.createContext();
const { Suspense } = React;

function App() {
  const [queryRef, loadQuery] = useQueryLoader(FeedQuery);
  const otpUserId = useRef();
  // update secure storage with
  const setLoginLocal = (loginData) => {
    try {
      var accessToken = loginData?.accessToken;
      var userId = loginData?.id;
      if (accessToken && userId) {
        return [
          SecureStore.setItemAsync(keyAccessToken, accessToken),
          SecureStore.setItemAsync(keyUserId, userId),
        ];
      }
    } catch (err) {
      return Promise.reject();
    }
  };

  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case "RESTORE_TOKEN":
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case "SIGN_IN":
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case "SIGN_OUT":
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    SecureStore.getItemAsync(keyAccessToken)
      .then((value) => {
        if (value) {
          // start loading data if token exist, NOTE : always call before dispatch, otherwise it may cause dump, if view loaded before loadQuery
          loadQuery({
            count: INITAIL_PAGINATION_COUNT,
            after: 0,
          });
          dispatch({ type: "RESTORE_TOKEN", token: value });
        } else {
          dispatch({ type: "RESTORE_TOKEN", token: null });
        }
      })
      .catch((error) => {
        dispatch({ type: "RESTORE_TOKEN", token: null });
      });
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async (data) => {
        DeviceEventEmitter.emit("loginError", {
          loading: true,
          display: false,
          msg: "",
        });
        axios
          .post(`/user/login`, {
            username: data?.email,
            password: data?.password,
            appId: Constants.manifest.android.package,
          })
          .then((response) => {
            var userData = response?.data;
            if (userData) {
              Promise.all(setLoginLocal(userData))
                .then(() => {
                  loadQuery({
                    count: INITAIL_PAGINATION_COUNT,
                    after: 0,
                  });
                  dispatch({ type: "SIGN_IN", token: userData?.accessToken });
                })
                .catch((error) => {
                  dispatch({ type: "RESTORE_TOKEN", token: null });
                  DeviceEventEmitter.emit("loginError", {
                    loading: false,
                    display: true,
                    msg: "UserName and Passowrd doesn't match, Please try agian",
                  });
                });
            }
          })
          .catch((error) => {
            if (error.response) {
              DeviceEventEmitter.emit("loginError", {
                loading: false,
                display: true,
                msg: "UserName and Passowrd doesn't match, Please try agian",
              });
            } else {
              DeviceEventEmitter.emit("loginError", {
                loading: false,
                display: true,
                msg: "Check your internet connection and please try again",
              });
            }
          });
      },
      signOut: () => dispatch({ type: "SIGN_OUT" }),
      signUp: async (data) => {
        DeviceEventEmitter.emit("registerMsg", {
          loading: true,
          display: false,
          msg: "",
          borderColor: "",
          backgroundColor: "",
        });
        axios
          .post(`/user/register`, {
            firstName: data?.username,
            email: data?.email,
            password: data?.password,
            appId: Constants.manifest.android.package,
          })
          .then((response) => {
            var userData = response?.data;
            if (userData) {
              otpUserId.current = userData.userId;
              DeviceEventEmitter.emit("validateOtp", {
                data: userData.otpExpiryTime,
              });
            }
          })
          .catch((error) => {
            if (error.response) {
              DeviceEventEmitter.emit("registerMsg", {
                loading: false,
                display: true,
                msg: error.response.data?.message
                  ? error.response.data?.message
                  : "Unable to register your account, please try again in some time",
                borderColor: "",
                backgroundColor: "",
              });
            } else {
              DeviceEventEmitter.emit("loginError", {
                loading: false,
                display: true,
                msg: "Check your internet connection and please try again",
              });
            }
          });
      },
      validateOtp: async (data) => {
        DeviceEventEmitter.emit("registerMsg", {
          loading: true,
          display: false,
          msg: "",
          borderColor: "",
          backgroundColor: "",
        });
        axios
          .post(`/user/otp`, {
            id: otpUserId.current,
            otp: data?.otp,
          })
          .then((response) => {
            var userData = response?.data;
            if (userData) {
              Promise.all(setLoginLocal(userData))
                .then(() => {
                  loadQuery({
                    count: INITAIL_PAGINATION_COUNT,
                    after: 0,
                  });
                  dispatch({ type: "SIGN_IN", token: userData?.accessToken });
                })
                .catch((error) => {
                  dispatch({ type: "RESTORE_TOKEN", token: null });
                  DeviceEventEmitter.emit("registerMsg", {
                    loading: false,
                    display: true,
                    msg: "OTP is not valid, please try again",
                  });
                });
            }
          })
          .catch((error) => {
            if (error.response) {
              DeviceEventEmitter.emit("registerMsg", {
                loading: false,
                display: true,
                msg: error.response.data?.message
                  ? error.response.data?.message
                  : "OTP is not valid, please try again",
                borderColor: "",
                backgroundColor: "",
              });
            } else {
              DeviceEventEmitter.emit("loginError", {
                loading: false,
                display: true,
                msg: "Check your internet connection and please try again",
              });
            }
          });
      },
      resendOtp: async () => {
        DeviceEventEmitter.emit("registerMsg", {
          loading: true,
          display: false,
          msg: "",
          borderColor: "",
          backgroundColor: "",
        });
        axios
          .get(`/user/otp/${otpUserId.current}`)
          .then((response) => {
            DeviceEventEmitter.emit("registerMsg", {
              loading: false,
              display: true,
              msg: "OTP has resent to your mail, Enter OTP to continue",
              borderColor: "#414ee8",
              backgroundColor: "#636ff4",
            });
          })
          .catch((error) => {
            if (error.response) {
              DeviceEventEmitter.emit("registerMsg", {
                loading: false,
                display: true,
                msg: error.response.data?.message
                  ? error.response.data?.message
                  : "We are facing some problem to generate your OTP, please try again",
                borderColor: "",
                backgroundColor: "",
              });
            } else {
              DeviceEventEmitter.emit("registerMsg", {
                loading: false,
                display: true,
                msg: "Check your internet connection and please try again",
              });
            }
          });
      },
    }),
    []
  );
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <SafeAreaProvider style={{ width: "100%" }}>
        <MutationPageLoader />
        <NavigationContainer>
          <AuthContext.Provider value={authContext}>
            {state.userToken !== null ? (
              <Stack.Navigator
                initialRouteName="Home"
                mode="modal"
                screenOptions={{
                  headerStyle: {
                    backgroundColor: AppColor.Vibrant,
                  },
                  gestureResponseDistance: { vertical: 1000 },
                }}
              >
                <Stack.Screen
                  name="Home"
                  component={Home}
                  initialParams={{
                    AppColor: AppColor,
                    queryRef: queryRef,
                    AuthContext: AuthContext,
                  }}
                  options={{ headerShown: false }}
                />
                <Stack.Screen
                  name="Search"
                  component={SearchQuery}
                  options={{
                    headerShown: false,
                    cardStyle: { backgroundColor: "transparent" },
                  }}
                />
                <Stack.Screen
                  name="SearchNavigator"
                  component={SearchNavigator}
                  options={{
                    headerShown: false,
                    cardStyle: { backgroundColor: "transparent" },
                  }}
                />
                <Stack.Screen
                  name="PaymentPage"
                  component={PaymentPage}
                  options={{
                    title: "Payment Details",
                    headerBackTitle: "Order",
                    headerTitleStyle: { color: AppColor.White },
                  }}
                />
              </Stack.Navigator>
            ) : (
              <Stack.Navigator
                initialRouteName="Login"
                screenOptions={{
                  headerShown: false,
                }}
              >
                <Stack.Screen
                  name="Login"
                  component={Login}
                  initialParams={{
                    AuthContext: AuthContext,
                  }}
                />
                <Stack.Screen
                  name="Register"
                  component={Register}
                  initialParams={{
                    AuthContext: AuthContext,
                  }}
                />
              </Stack.Navigator>
            )}
          </AuthContext.Provider>
        </NavigationContainer>
      </SafeAreaProvider>
    </>
  );
}

function AppRoot(props) {
  return (
    <ApplicationProvider {...eva} theme={eva.light} style={{ width: "100%" }}>
      <RelayEnvironmentProvider environment={RelayEnvironment}>
        <Suspense fallback={<HomePageLoader />}>
          <AnimatedAppLoader image={{ uri: Constants.manifest.splash.image }}>
            <App />
          </AnimatedAppLoader>
        </Suspense>
      </RelayEnvironmentProvider>
    </ApplicationProvider>
  );
}

export default AppRoot;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
